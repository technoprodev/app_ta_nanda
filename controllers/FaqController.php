<?php
namespace app_ta_nanda\controllers;

use Yii;
use technosmart\yii\web\Controller;
use technosmart\models\Faq;

class FaqController extends Controller
{
	public function actionIndex()
	{
		$model['faq'] = Faq::find()->asArray()->all();
		return $this->render('list', [
			'model' => $model,
		]);
	}   
}