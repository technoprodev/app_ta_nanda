<?php
namespace app_ta_nanda\controllers;

use Yii;
use technosmart\controllers\SiteController as SiteControl;
use app_ta_nanda\models\UserIdentity;
use app_ta_nanda_admin\models\Customer;
use technosmart\models\Post;

class SiteController extends SiteControl
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'register', 'login', 'error'], true, ['?', '@'], ['GET', 'POST']],
                [['logout'], true, ['@'], ['POST']],
            ]),
        ];
    }

    public function actionIndex()
    {
        // $this->layout = 'main-blog';
        // return $this->render('index-blog');
        $model['posts'] = Post::find()->orderBy(['id' => SORT_DESC])->all();
        
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if ( !Yii::$app->request->isPost || !($model['userIdentity'] = UserIdentity::findByLogin(Yii::$app->request->post()['UserIdentity']['login'])) )
            $model['userIdentity'] = new UserIdentity();
        $model['userIdentity']->scenario = 'login';

        if ($model['userIdentity']->load(Yii::$app->request->post()) && $model['userIdentity']->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'empty';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $render = false;

        $model['userIdentity'] = new UserIdentity();
        $model['userIdentity']->scenario = 'pass';
        $model['customer'] = new Customer();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['userIdentity']->load($post);
            $model['customer']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['userIdentity']),
                    ActiveForm::validate($model['customer'])
                );
                return $this->json($result);
            }

            $transaction['userIdentity'] = UserIdentity::getDb()->beginTransaction();
            $transaction['customer'] = Customer::getDb()->beginTransaction();

            try {
                if (!$model['userIdentity']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                $model['customer']->id = $model['userIdentity']->id;
                $model['customer']->email = $model['userIdentity']->email;
                if (!$model['customer']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['userIdentity']->commit();
                $transaction['customer']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['userIdentity']->rollBack();
                $transaction['customer']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['userIdentity']->rollBack();
                $transaction['customer']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render) {
            $this->layout = 'empty';
            return $this->render('register', [
                'model' => $model,
            ]);
        }
        else {
            Yii::$app->session->setFlash('success', 'Registrasi berhasil. Silahkan login.');
            return $this->goBack();
        }
    }
}