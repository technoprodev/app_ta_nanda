<?php
namespace app_ta_nanda\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_ta_nanda_admin\models\Transaction;
use yii\widgets\ActiveForm;

class BookingController extends Controller
{
    public function actionIndex()
    {
        $render = false;

        $model['transaction'] = new Transaction();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaction']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['transaction'])
                );
                return $this->json($result);
            }

            $transaction['transaction'] = Transaction::getDb()->beginTransaction();

            try {
                if ($model['transaction']->isNewRecord) {
                    $model['transaction']->id_customer = Yii::$app->user->identity->id;
                    $model['transaction']->name = Yii::$app->user->identity->name;
                    $model['transaction']->email = Yii::$app->user->identity->email;
                    $model['transaction']->booking_date = new \yii\db\Expression("now()");
                }
                if (!$model['transaction']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['transaction']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['transaction']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['transaction']->rollBack();
            }
        } else {
            if (!Yii::$app->user->isGuest) {
                $model['transaction']->phone = Yii::$app->user->identity->customer->phone;
            }
            $render = true;
        }

        if ($render)
            return $this->render('index', [
                'model' => $model,
                'title' => 'Add Booking',
            ]);
        else {
            Yii::$app->session->setFlash('success', 'Proses booking berhasil. Harap tunggu CS kami mengontak Anda.');
            return $this->redirect(['success', 'id' => $model['transaction']->id]);
        }
    }

    public function actionSuccess($id)
    {
        $this->layout = 'empty';
        $model['transaction'] = Transaction::findOne($id);
        return $this->render('success', [
            'model' => $model,
            'title' => 'Bukti Booking',
        ]);       

    }
}