<?php
namespace app_ta_nanda\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_ta_nanda_admin\models\Customer;
use app_ta_nanda\models\UserIdentity;
use yii\widgets\ActiveForm;

class ProfileController extends Controller
{
    public function actionIndex()
    {
        $model['customer'] = $this->findModel(Yii::$app->user->identity->id);
        $model['userIdentity'] = $this->findUserIdentity(Yii::$app->user->identity->id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Profile ' . $model['userIdentity']->name,
        ]);
    }

    public function actionUpdate($id = null)
    {
        $render = false;

        $model['customer'] = $this->findModel(Yii::$app->user->identity->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['customer']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['customer'])
                );
                return $this->json($result);
            }

            $transaction['customer'] = Customer::getDb()->beginTransaction();

            try {
                if ($model['customer']->isNewRecord) {}
                if (!$model['customer']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['customer']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['customer']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['customer']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Edit Profile',
            ]);
        else
            return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findUserIdentity($id)
    {
        if (($model = UserIdentity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}