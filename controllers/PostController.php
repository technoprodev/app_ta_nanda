<?php
namespace app_ta_nanda\controllers;

use Yii;
use technosmart\yii\web\Controller;
use technosmart\models\Post;
use technosmart\models\PostCategory;

class PostController extends Controller
{
    public function actionIndex($id = null, $code = null)
    {
        if (isset($code)) {
            return $this->render($code);
        }
        
        if (isset($id)) {
            $model['post'] = $this->findModel($id);
            $model['posts'] = Post::find()->orderBy(['id' => SORT_DESC])->where(['<>', 'id', $id])->all();
            return $this->render('one', [
                'model' => $model,
                'title' => 'Detail of Post ' . $model['post']->id,
            ]);
        }
    }

    public function actionCategory($id)
    {
        $model['postCategory'] = PostCategory::find()->where(['id' => $id])->one();
        $model['post'] = Post::find()->where(['id_post_category' => $id])->all();
        return $this->render('list', [
            'title' => 'List of Posts',
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}