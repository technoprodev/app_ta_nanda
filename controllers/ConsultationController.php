<?php
namespace app_ta_nanda\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_ta_nanda\models\Consultation;

class ConsultationController extends Controller
{
    public function actionIndex()
    {
        $model['consultation'] = new Consultation();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['consultation']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['consultation'])
                );
                return $this->json($result);
            }

            // try {
                Consultation::sendEmail($model);

                Yii::$app->session->setFlash('success', 'Pesan Anda berhasil dikirim. Tim Kami akan segera merespon pesan Anda.');
                return $this->redirect(['index']);
            /*} catch (\Exception $e) {
            } catch (\Throwable $e) {
            }*/
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}