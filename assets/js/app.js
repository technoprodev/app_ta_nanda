if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            dev: {
                text: null,
                virtualCategory: [],
                devChildren: {
                    new: [],
                },
            },
        },
        methods: {
            addChild: function() {
                var newDevChildren = {
                    id: null,
                    id_dev: null,
                    child: null
                };
                this.dev.devChildren['new'].push(newDevChildren);
            },
        },
    });
}