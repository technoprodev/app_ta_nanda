<?php
namespace app_ta_nanda\models;

use Yii;
use app_ta_nanda_admin\models\Customer;

/**
 * This is connector for Yii::$app->user with model User
 *
 * @property integer $id
 * @property string $username
 */
class UserIdentity extends \technosmart\models\User
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            // [['name'], 'required'],
            // [['email'], 'required'],
        ]);
    }

	public static function findByLogin($login)
    {
    	$userIdentity = static::find()
            ->where('username = :login or email = :login', [':login' => $login])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->one();

        $found = $userIdentity && Customer::findOne(['id' => $userIdentity->id]);

        if ($found) return $userIdentity;
        else return null;
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id']);
    }
}