<?php
namespace app_ta_nanda\models;

use Yii;

/**
 * This is the model class for table "dev".
 *
 * @property integer $id
 * @property string $id_combination
 * @property string $id_user_defined
 * @property string $text
 * @property integer $link
 * @property string $enum
 * @property string $set
 * @property string $file
 *
 * @property DevCategoryOption $link0
 * @property DevCategory[] $devCategories
 * @property DevChild[] $devChildren
 * @property DevExtend $devExtend
 */
class Consultation extends \yii\base\Model
{
    public $name;
    public $email;
    public $type;
    public $message;

    public function rules()
    {
        return [
            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],

            //email
            [['email'], 'trim'],
            [['email'], 'required'],
            [['email'], 'string', 'max' => 64],
            [['email'], 'email'],
            
            //type
            [['type'], 'safe'],

            //message
            [['message'], 'safe'],
        ];
    }

    public static function sendEmail($model)
    {
        \Yii::$app->mail->compose('consultation-index', ['model' => $model])
            ->setFrom($model['consultation']->email)
            ->setTo('pradana.fandy@gmail.com')
            ->setSubject('Anugerah Satwa - Konsultasi (' . $model['consultation']->type . ')')
            ->send();

        \Yii::$app->mail->compose('consultation-index', ['model' => $model])
            ->setFrom($model['consultation']->email)
            ->setTo('febrianda.waldi@gmail.com')
            ->setSubject('Anugerah Satwa - Konsultasi (' . $model['consultation']->type . ')')
            ->send();
    }
}
