<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = 'Booking';
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

$error = false;
$errorMessage = '';
if ($model['transaction']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['transaction'], ['class' => '']);
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30 margin-bottom-20">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">Booking</span>
            </div>

            <?php if (Yii::$app->user->isGuest) : ?>
                <div class="padding-15 bg-light-red border-red rounded-xs">Harap login terlebih dahulu untuk mengakses menu ini.</div>
            <?php else : ?>
                <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
                    <?php if ($error) : ?>
                        <div class="alert alert-danger">
                            <?= $errorMessage ?>
                        </div>
                    <?php endif; ?>

                    <div class="box">
                        <div class="box-6 padding-x-0">
                            <?= $form->field($model['transaction'], 'phone')->begin(); ?>
                                <?= Html::activeLabel($model['transaction'], 'phone', ['class' => 'control-label']); ?>
                                <?= Html::activeTextInput($model['transaction'], 'phone', ['class' => 'form-control', 'maxlength' => true]) ?>
                                <?= Html::error($model['transaction'], 'phone', ['class' => 'help-block']); ?>
                            <?= $form->field($model['transaction'], 'phone')->end(); ?>
                        </div>
                        <div class="box-6 padding-right-0 m-padding-x-0">
                            <?= $form->field($model['transaction'], 'appoinment_date')->begin(); ?>
                                <?= Html::activeLabel($model['transaction'], 'appoinment_date', ['class' => 'control-label']); ?>
                                <?= Html::activeTextInput($model['transaction'], 'appoinment_date', ['class' => 'form-control input-mask-date input-datepicker']) ?>
                                <?= Html::error($model['transaction'], 'appoinment_date', ['class' => 'help-block']); ?>
                            <?= $form->field($model['transaction'], 'appoinment_date')->end(); ?>
                        </div>
                    </div>

                    <?= $form->field($model['transaction'], 'service_type')->begin(); ?>
                        <?= Html::activeLabel($model['transaction'], 'service_type', ['class' => 'control-label']); ?>
                        <?= Html::activeDropDownList($model['transaction'], 'service_type', [ 'klinik' => 'Klinik', 'petshop' => 'Petshop', 'grooming' => 'Grooming', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
                        <?= Html::error($model['transaction'], 'service_type', ['class' => 'help-block']); ?>
                    <?= $form->field($model['transaction'], 'service_type')->end(); ?>

                    <?= $form->field($model['transaction'], 'service_name')->begin(); ?>
                        <?= Html::activeLabel($model['transaction'], 'service_name', ['class' => 'control-label']); ?>
                        <?= Html::activeTextInput($model['transaction'], 'service_name', ['class' => 'form-control', 'maxlength' => true]) ?>
                        <?= Html::error($model['transaction'], 'service_name', ['class' => 'help-block']); ?>
                    <?= $form->field($model['transaction'], 'service_name')->end(); ?>

                    <?= $form->field($model['transaction'], 'note')->begin(); ?>
                        <?= Html::activeLabel($model['transaction'], 'note', ['class' => 'control-label']); ?>
                        <?= Html::activeTextArea($model['transaction'], 'note', ['class' => 'form-control', 'rows' => 6]) ?>
                        <?= Html::error($model['transaction'], 'note', ['class' => 'help-block']); ?>
                    <?= $form->field($model['transaction'], 'note')->end(); ?>


                    <hr class="margin-y-15">

                    <?php if ($error) : ?>
                        <div class="alert alert-danger">
                            <?= $errorMessage ?>
                        </div>
                    <?php endif; ?>
                    
                    <div class="form-group clearfix">
                        <?= Html::submitButton($model['transaction']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
                        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
                    </div>
                    
                <?php ActiveForm::end(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>