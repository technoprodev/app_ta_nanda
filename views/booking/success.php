<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = 'Booking';
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);
?>

<div class="container">
    <div class="row margin-x-0">
        <div class="col-md-6 bg-lightest shadow margin-top-50">
            <div class="padding-y-30 margin-bottom-20">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">Bukti Booking</span>
            </div>

            <?php if (Yii::$app->user->isGuest) : ?>
                <div class="padding-15 bg-light-red border-red rounded-xs">Harap login terlebih dahulu untuk mengakses menu ini.</div>
            <?php else : ?>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-3 m-padding-x-0 text-right m-text-left">Nama Pelanggan</div>
                    <div class="box-9 m-padding-x-0 text-dark"><?= $model['transaction']->name ? $model['transaction']->name : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-3 m-padding-x-0 text-right m-text-left">No HP</div>
                    <div class="box-9 m-padding-x-0 text-dark"><?= $model['transaction']->phone ? $model['transaction']->phone : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-3 m-padding-x-0 text-right m-text-left">Email</div>
                    <div class="box-9 m-padding-x-0 text-dark"><?= $model['transaction']->email ? $model['transaction']->email : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-3 m-padding-x-0 text-right m-text-left">Jenis Layanan</div>
                    <div class="box-9 m-padding-x-0 text-dark"><?= $model['transaction']->service_type ? $model['transaction']->service_type : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-3 m-padding-x-0 text-right m-text-left">Nama Layanan</div>
                    <div class="box-9 m-padding-x-0 text-dark"><?= $model['transaction']->service_name ? $model['transaction']->service_name : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-3 m-padding-x-0 text-right m-text-left">Tanggal Booking</div>
                    <div class="box-9 m-padding-x-0 text-dark"><?= $model['transaction']->booking_date ? $model['transaction']->booking_date : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-3 m-padding-x-0 text-right m-text-left">Tanggal Janji Kunjungan</div>
                    <div class="box-9 m-padding-x-0 text-dark"><?= $model['transaction']->appoinment_date ? $model['transaction']->appoinment_date : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-3 m-padding-x-0 text-right m-text-left">Catatan</div>
                    <div class="box-9 m-padding-x-0 text-dark"><?= $model['transaction']->note ? $model['transaction']->note : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
                </div>

                <dir class="margin-y-30 padding-10 f-italic">
                    Harap bawa bukti ini ketika Anda datang ke Anugerah Satwa, terima kasih.
                </dir>

            <?php endif; ?>
        </div>
    </div>
    <div class="margin-top-50">
        <a class="btn btn-default rounded-xs text-gray" href="<?= Yii::$app->urlManager->createUrl("site/index") ?>">Kembali ke Home</a>
        <a class="btn btn-default rounded-xs text-gray" href="<?= Yii::$app->urlManager->createUrl("booking/index") ?>">Kembali ke Halaman Booking</a>
    </div>
</div>