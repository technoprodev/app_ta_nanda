<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">FAQ</span>
            </div>

            <?php foreach ($model['faq'] as $key => $faq) : ?>
                <div class="margin-bottom-30 padding-left-10">
                    <h6><i class="fa fa-caret-right text-azure"></i> <?= $faq['question'] ?></h6>
                    <p class="fs-14"><?= $faq['answer'] ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>