<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30 margin-bottom-20">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom"><?= $title ?></span>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['email'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->email ? $model['customer']->email : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['phone'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->phone ? $model['customer']->phone : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['pet_1'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->pet_1 ? $model['customer']->pet_1 : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['pet_2'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->pet_2 ? $model['customer']->pet_2 : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['pet_3'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->pet_3 ? $model['customer']->pet_3 : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['alamat'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->alamat ? $model['customer']->alamat : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['birthplace'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->birthplace ? $model['customer']->birthplace : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['birthday'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->birthday ? $model['customer']->birthday : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['relative_name'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->relative_name ? $model['customer']->relative_name : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
                
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['customer']->attributeLabels()['relative_email'] ?></div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['customer']->relative_email ? $model['customer']->relative_email : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
            </div>
    
            <hr class="margin-y-15">
            <div class="form-group clearfix">
                <?= Html::a('Edit Profile', ['update'], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>
            </div>
        </div>
    </div>
</div>
