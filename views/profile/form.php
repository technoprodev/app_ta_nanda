<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['customer']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['customer'], ['class' => '']);
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30 margin-bottom-20">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom"><?= $title ?></span>
            </div>

            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
              
                <?php if ($error) : ?>
                    <div class="alert alert-danger">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <?= $form->field($model['customer'], 'email')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'email', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['customer'], 'email', ['class' => 'form-control', 'maxlength' => true]) ?>
                    <?= Html::error($model['customer'], 'email', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'email')->end(); ?>

                <?= $form->field($model['customer'], 'phone')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'phone', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['customer'], 'phone', ['class' => 'form-control', 'maxlength' => true]) ?>
                    <?= Html::error($model['customer'], 'phone', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'phone')->end(); ?>

                <?= $form->field($model['customer'], 'pet_1')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'pet_1', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['customer'], 'pet_1', ['class' => 'form-control', 'maxlength' => true]) ?>
                    <?= Html::error($model['customer'], 'pet_1', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'pet_1')->end(); ?>

                <?= $form->field($model['customer'], 'pet_2')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'pet_2', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['customer'], 'pet_2', ['class' => 'form-control', 'maxlength' => true]) ?>
                    <?= Html::error($model['customer'], 'pet_2', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'pet_2')->end(); ?>

                <?= $form->field($model['customer'], 'pet_3')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'pet_3', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['customer'], 'pet_3', ['class' => 'form-control', 'maxlength' => true]) ?>
                    <?= Html::error($model['customer'], 'pet_3', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'pet_3')->end(); ?>

                <?= $form->field($model['customer'], 'alamat')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'alamat', ['class' => 'control-label']); ?>
                    <?= Html::activeTextArea($model['customer'], 'alamat', ['class' => 'form-control', 'rows' => 6]) ?>
                    <?= Html::error($model['customer'], 'alamat', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'alamat')->end(); ?>

                <?= $form->field($model['customer'], 'birthplace')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'birthplace', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['customer'], 'birthplace', ['class' => 'form-control', 'maxlength' => true]) ?>
                    <?= Html::error($model['customer'], 'birthplace', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'birthplace')->end(); ?>

                <?= $form->field($model['customer'], 'birthday')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'birthday', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['customer'], 'birthday', ['class' => 'form-control']) ?>
                    <?= Html::error($model['customer'], 'birthday', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'birthday')->end(); ?>

                <?= $form->field($model['customer'], 'relative_name')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'relative_name', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['customer'], 'relative_name', ['class' => 'form-control', 'maxlength' => true]) ?>
                    <?= Html::error($model['customer'], 'relative_name', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'relative_name')->end(); ?>

                <?= $form->field($model['customer'], 'relative_email')->begin(); ?>
                    <?= Html::activeLabel($model['customer'], 'relative_email', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['customer'], 'relative_email', ['class' => 'form-control', 'maxlength' => true]) ?>
                    <?= Html::error($model['customer'], 'relative_email', ['class' => 'help-block']); ?>
                <?= $form->field($model['customer'], 'relative_email')->end(); ?>


                <hr class="margin-y-15">

                <?php if ($error) : ?>
                    <div class="alert alert-danger">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-group clearfix">
                    <?= Html::submitButton($model['customer']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
                    <?= Html::a('Back to Profile', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
                </div>
                
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>