<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['contact_us']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['contact_us'], ['class' => '']);
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30 margin-bottom-20">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">Contact Us</span>
            </div>

            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <?php if ($error) : ?>
                <div class="alert alert-danger">
                    <?= $errorMessage ?>
                </div>
            <?php endif; ?>

            <div class="box box-break-sm">
                <div class="box-6 m-padding-0 padding-left-0">
                    <?= $form->field($model['contact_us'], 'name')->begin(); ?>
                        <?= Html::activeLabel($model['contact_us'], 'name', ['class' => 'control-label']); ?>
                        <?= Html::activeTextInput($model['contact_us'], 'name', ['class' => 'form-control', 'maxlength' => true]) ?>
                        <?= Html::error($model['contact_us'], 'name', ['class' => 'help-block']); ?>
                    <?= $form->field($model['contact_us'], 'name')->end(); ?>
                </div>

                <div class="box-6 m-padding-0 padding-right-0">
                    <?= $form->field($model['contact_us'], 'email')->begin(); ?>
                        <?= Html::activeLabel($model['contact_us'], 'email', ['class' => 'control-label']); ?>
                        <?= Html::activeTextInput($model['contact_us'], 'email', ['class' => 'form-control', 'maxlength' => true]) ?>
                        <?= Html::error($model['contact_us'], 'email', ['class' => 'help-block']); ?>
                    <?= $form->field($model['contact_us'], 'email')->end(); ?>
                </div>
            </div>

            <?= $form->field($model['contact_us'], 'subject')->begin(); ?>
                <?= Html::activeLabel($model['contact_us'], 'subject', ['class' => 'control-label']); ?>
                <?= Html::activeTextInput($model['contact_us'], 'subject', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['contact_us'], 'subject', ['class' => 'help-block']); ?>
            <?= $form->field($model['contact_us'], 'subject')->end(); ?>

            <?= $form->field($model['contact_us'], 'message')->begin(); ?>
                <?= Html::activeLabel($model['contact_us'], 'message', ['class' => 'control-label']); ?>
                <?= Html::activeTextArea($model['contact_us'], 'message', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['contact_us'], 'message', ['class' => 'help-block']); ?>
            <?= $form->field($model['contact_us'], 'message')->end(); ?>

            <div class="form-group clearfix">
                <?= Html::submitButton('Send', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>