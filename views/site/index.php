<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['slider'] = true;
?>

<div class="bg-lighter">
    <div class="container padding-top-30 padding-bottom-50">
        <div class="margin-y-20 text-center fs-30 f-bold text-azure">
            Featured Service
        </div>
        <div class="box box-bream-sm box-space-md">
            <div class="box-4 rounded-xs hover-bg-dark bg-azure padding-x-30 padding-top-30 padding-bottom-20">
                <div class="text-center fs-18 margin-bottom-10">
                    Klinik
                </div>
                <div class="fs-14 margin-bottom-10">
                    Dengan sistem yang handal, kami siap menolong hewan kesayangan Anda, agar tetap sehat.
                </div>
                <div class="text-center fs-38">
                    <i class="fa fa-check-circle"></i>
                </div>
            </div>
            <div class="box-4 rounded-xs hover-bg-dark bg-azure padding-x-30 padding-top-30 padding-bottom-20">
                <div class="text-center fs-18 margin-bottom-10">
                    Pet Shop
                </div>
                <div class="fs-14 margin-bottom-10">
                    Hewan yang menggemaskan perlu asupan gizi terbaik. Dapatkan kualitas makanan terbaik dengan harga yang murah disini.
                </div>
                <div class="text-center fs-38">
                    <i class="fa fa-check-circle"></i>
                </div>
            </div>
            <div class="box-4 rounded-xs hover-bg-dark bg-azure padding-x-30 padding-top-30 padding-bottom-20">
                <div class="text-center fs-18 margin-bottom-10">
                    Grooming
                </div>
                <div class="fs-14 margin-bottom-10">
                    Hewan juga membutuhkan perawatan. Percayakan pada kami yang berpengalaman lebih dari 10 tahun.
                </div>
                <div class="text-center fs-38">
                    <i class="fa fa-check-circle"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-gray">
    <div class="container padding-top-30 padding-bottom-50">
        <div class="margin-y-20 text-center fs-30 f-bold">
            Jam Kerja
        </div>
        <table class="table table-bordered bg-lightest rounded-xs margin-top-30 border-thin border-light text-center">
            <thead>
                <tr class="bg-azure">
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                    <th>Sunday</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>8:00 am</td>
                    <td>8:00 am</td>
                    <td>8:00 am</td>
                    <td>8:00 am</td>
                    <td>8:00 am</td>
                    <td>8:00 am</td>
                    <td>Closed</td>
                </tr>
                <tr>
                    <td>7:00 pm</td>
                    <td>7:00 pm</td>
                    <td>7:00 pm</td>
                    <td>7:00 pm</td>
                    <td>6:00 pm</td>
                    <td>2:00 pm</td>
                    <td>Closed</td>
                </tr>
            </tbody>
        </table>
        <div class="margin-top-10 fs-18 text-center">
            For after hours emergencies, please call (402) 597-2911.
        </div>
    </div>
</div>

<div class="bg-lightest">
    <div class="container padding-top-30 padding-bottom-50">
        <div class="margin-y-20 text-center fs-30 f-bold text-azure">
            Testimonial
        </div>
        <div class="box box-bream-sm box-space-md">
            <div class="box-4 rounded-xs hover-darker-10 padding-x-30 padding-top-30 padding-bottom-20">
                <div class="fs-16 f-italic text-center margin-bottom-10">
                    Saya berlangganan di Anugerah Satwa selama 11 tahun terakhir, pegawainya sangat ramah dan ahli dibidangnya, toko selalu bersih. Saya membeli persediaan kucing dari sini dan harganya sangat terjangkau. Anugerah Satwa selalu menjadi pilihan pertama saya. Saya tidak akan pergi ke tempat lain!
                </div>
                <div class="text-center fs-20 text-dark margin-top-30">
                    Eko Prasetyo
                </div>
                <div class="text-center fs-18">
                    Penggemar Kucing.
                </div>
            </div>
            <div class="box-4 rounded-xs hover-darker-10 padding-x-30 padding-top-30 padding-bottom-20">
                <div class="fs-16 f-italic text-center margin-bottom-10">
                    Pegawainya selalu bersedia membantu Anda menemukan produk yang tepat sesuai kebutuhan Anda. Ya, Anda membayar sedikit lebih banyak, tetapi jika Anda pergi ke WalMart, Anda tidak akan mendapatkan bantuan yang Anda inginkan dalam membeli produk. Anak-anak anjing selalu terlihat diurus dengan baik dan bersih. Mereka bahkan akan membiarkan Anda bermain dengan mereka!
                </div>
                <div class="text-center fs-20 text-dark margin-top-30">
                    Ganang Arief
                </div>
                <div class="text-center fs-18">
                    Pecinta Anjing.
                </div>
            </div>
            <div class="box-4 rounded-xs hover-darker-10 padding-x-30 padding-top-30 padding-bottom-20">
                <div class="fs-16 f-italic text-center margin-bottom-10">
                    Halo Mas :) anjing-anjing saya sekarang sehat semua, semuanya aktif bermain :) Saya akan selalu merekomendasikan Anda kepada siapa saja yang saya kenal, hahaha. Sekali lagi terima kasih banyak.
                </div>
                <div class="text-center fs-20 text-dark margin-top-30 hover-text-azure">
                    Fandy Pradana
                </div>
                <div class="text-center fs-18">
                    Pemilik Anjing.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-lighter">
    <div class="container padding-top-30 padding-bottom-50">
        <div class="margin-y-20 text-center fs-30 f-bold text-azure">
            Artikel
        </div>
        <div class="box margin-bottom-30 equal">
        <?php foreach ($model['posts'] as $key => $post) : ?>
            <div class="box-4 padding-left-0 padding-right-20">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/cover.jpg" class="rounded-xs" style="width: 100%; height: auto;">
                <div class="fs-15 margin-top-10">
                    <?= Html::a($post->title, ['post/index', 'id' => $post->id], ['class' => 'text-grayest ']) ?>
                </div>
                <div class="text-gray fs-12 margin-top-5">
                    by <?= $post->createdBy->name ?> on <?= $post->created_at ?>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</div>