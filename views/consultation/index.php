<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30 margin-bottom-20">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">Konsultasi</span>
            </div>

            <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>

            <div class="box box-break-sm">
            	<div class="box-6 m-padding-0 padding-left-0">
		            <?= $form->field($model['consultation'], 'name')->begin(); ?>
				        <?= Html::activeLabel($model['consultation'], 'name', ['class' => 'control-label']); ?>
				        <?= Html::activeTextInput($model['consultation'], 'name', ['class' => 'form-control', 'maxlength' => true]) ?>
				        <?= Html::error($model['consultation'], 'name', ['class' => 'help-block']); ?>
				    <?= $form->field($model['consultation'], 'name')->end(); ?>
            	</div>

            	<div class="box-6 m-padding-0 padding-right-0">
				    <?= $form->field($model['consultation'], 'email')->begin(); ?>
				        <?= Html::activeLabel($model['consultation'], 'email', ['class' => 'control-label']); ?>
				        <?= Html::activeTextInput($model['consultation'], 'email', ['class' => 'form-control', 'maxlength' => true]) ?>
				        <?= Html::error($model['consultation'], 'email', ['class' => 'help-block']); ?>
				    <?= $form->field($model['consultation'], 'email')->end(); ?>
            	</div>
            </div>

		    <?= $form->field($model['consultation'], 'type', ['options' => ['class' => 'form-group radio-elegant']])->begin(); ?>
		        <?= Html::activeLabel($model['consultation'], 'type', ['class' => 'control-label']); ?>
		        <?= Html::activeRadioList($model['consultation'], 'type', ['Konsultasi' => 'Konsultasi', 'Bertanya' => 'Bertanya', 'Lainnya' => 'Lainnya'], ['class' => 'row row-radio', 'unselect' => null,
		            'item' => function($index, $label, $name, $checked, $value){
		                $disabled = in_array($value, ['val1', 'val2', '1']) ? 'disabled' : '';
		                return "<div class='radio col-xs-3'><label><input type='radio' name='$name' value='$value' $disabled><i></i>$label</label></div>";
		            }]); ?>
		        <?= Html::error($model['consultation'], 'type', ['class' => 'help-block']); ?>
		    <?= $form->field($model['consultation'], 'type')->end(); ?>

		    <?= $form->field($model['consultation'], 'message')->begin(); ?>
		        <?= Html::activeLabel($model['consultation'], 'message', ['class' => 'control-label']); ?>
		        <?= Html::activeTextArea($model['consultation'], 'message', ['class' => 'form-control', 'maxlength' => true]) ?>
		        <?= Html::error($model['consultation'], 'message', ['class' => 'help-block']); ?>
		    <?= $form->field($model['consultation'], 'message')->end(); ?>

		    <div class="form-group clearfix">
		        <?= Html::submitButton('Send', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
		        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
		    </div>

			<?php ActiveForm::end(); ?>

        </div>
	</div>
</div>