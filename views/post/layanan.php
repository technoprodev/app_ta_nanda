<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = 'Layanan';
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">Layanan</span>
            </div>

            <div class="margin-bottom-30 padding-left-10">
                <h5><i class="fa fa-caret-right text-azure"></i> Konsultasi</h5>
                <p>Kami melayani konsultasi tentang hewan kesayangan dengan detail oleh tenaga medis profesional, kita bisa mengetahui dengan benar dan sepuasnya tentang segala sesuatu yang berhubungan dengan informasi kesehatan, pemeliharaan, dan langkah – langkah pencegahan terhadap penyakit – penyakit pada hewan.</p>
                <p>Konsultasi juga dilakukan ketika proses pemeriksaan kesehatan di poliklinik oleh dokter hewan yang sedang bertugas, kita bisa dengan bebas dan sepuas-puasnya bertanya tentang segala sesuatu yang pingin kita ketahui tentang hewan kesayangan kita seperti program vaksinasi yang benar, periode pemberian obat cacing, jadwal general cek up untuk hewan, pola pemberian pakan yang benar, perawatan mandi dan lain – lain.</p>

                <h5><i class="fa fa-caret-right text-azure"></i> Vaksinasi</h5>
                <p>Kami melayani vaksinasi pada anjing dan kucing setiap hari. Program Vaksinasi sesuai aturan penggunaan vaksin sebagaimana mestinya dan tentu disesuaikan dengan kondisi iklim kita.</p>
                <p>Kami melayani vaksinasi pada anjing dan kucing setiap hari. Program Vaksinasi sesuai aturan penggunaan vaksin sebagaimana mestinya dan tentu disesuaikan dengan kondisi iklim kita.</p>

                <h5><i class="fa fa-caret-right text-azure"></i> Sterilisasi Rutin</h5>
                <p>Sterilisasi pada anjing dan kucing dilakukan di vitapet animal clinic dengan tujuan untuk mengontrol populasi jumlah hewan, mencegah timbulnya cancer, penyakit disaluran reproduksi dan mengurangi perilaku “agresif atau kencing dimana-mana” yang sering terlihat pada hewan kesayangan kita. Di vitapet animal clinic semua prosedur sterilisasi dilakukan dibawah pengaruh obat bius total (Under General Anasthesia) sehingga hewan tidak mengalami rasa sakit ketika dilakukan operasi sterilisasi.</p>

                <h5><i class="fa fa-caret-right text-azure"></i> Diagnosa Penunjang Radiography</h5>
                <p>Vitapet Animal Clinic memiliki fasilitas semi digital radiography sebagai alat penunjang untuk membantu menentukan suatu diagnosa penyakit pada hewan kesayangan. Kami memiliki Universal semi digital X-Ray khusus hewan. Keuntungan alat ini adalah waktu proses rontgen yang relatif singkat (hanya sekitar 20 menit), kualitas hasil foto x-ray yang bagus (diagnostic) dan radiasi nya kecil. Ruangan rontgen di klinik juga telah dilapisi oleh timbal (Pb) sehingga pengaruh radiasi tidak kemana-mana, jadi sangat aman sekali.</p>

                <h5><i class="fa fa-caret-right text-azure"></i> Operasi : Soft Tissue Surgery dan Orthopaedic Surgery</h5>
                <p>Kami perform dengan berbagai kasus operasi di klinik, diantaranya operasi jaringan lunak (soft tissue) dan orhopedik. Vitapet Animal Clinic mempunyai fasilitas alat – alat operasi dengan sangat lengkap menggunakan produk Eickemeyer@. Soft tissue yang sudah kita lakukan seperti tumor mamae (mastectomy), hernia, pyometra, cystotomy, gastrotomy, enterectoy, enterotomy, spleenectomy, perineal urethrostomy dan lain – lain. Sedangkan Orthopedik yang sudah kita lakukan diklinik adalah fraktur kaki depan, kaki belakang, fraktur rahang, dan Hip (pinggul).</p>

                <h5><i class="fa fa-caret-right text-azure"></i> Rawat Inap Sehat</h5>
                <p>Rawat Inap di Vitapet Animal Clinic cukup besar dan kandang yang cukup luas dan nyaman. Kami melayani rawat inap karena sudah dilengkapi dengan fasilitas rawat inap lengkap termasuk segala kebutuhan obat – obatan. Kita juga dilengkapi incubator khusus hewan, lampu penghangat dan lain – lain. Rawat inap sehat hanya diperuntukkan untuk kasus paska operasi (dalam tahap recovery) dan hewan sakit yang bukan menular (Non – Contagius Diseases)</p>

                <h5><i class="fa fa-caret-right text-azure"></i> Unit Gawat Darurat (UGD)</h5>
                <p>Layanan emergency dibuka mulai pukul 00.00 sampai 08.00 Wib setiap hari, 7 hari seminggu non-stop. Vitapet Animal Clinic mempunyai fasilitas khusus ruang intensive care lengkap dengan alat – alat penunjang yang dibutuhkan selama perawata</p>

                <h5><i class="fa fa-caret-right text-azure"></i> Layanan Jemput-Antar</h5>
                <p>menyediakan layanan jemput-antar hewan, untuk pengobatan, dengan syarat berada di sekitar BSD</p>
            </div>
        </div>
    </div>
</div>