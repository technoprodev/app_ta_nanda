<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">About Us</span>
            </div>

            <div class="margin-bottom-20">
                <h5 class="f-bold">Profil Anugerah Satwa</h5>
                <p>Anugerah Satwa Pet Shop, Clinic, and Grooming adalah sebuah klinik hewan yang berlokasi di kota tanggerang yang berdiri sejak tahun 1998 hingga saat ini memiliki satu cabang yang terletak di Bumi Serpong Damai. Anugerah Satwa Pet Shop, Clinic, and Grooming menyediakan berbagai  jasa, seperti  pengobatan, vaksinasi, operasi, penitipan dan grooming terhadap hewan peliharaan masyarakat kota tanggerang</p>
            </div>

            <div class="margin-bottom-20">
                <h5 class="f-italic">Visi Anugerah Satwa</h5>
                <p><i class="fa fa-caret-right text-azure"></i> Menjadi klinik hewan unggulan di tanggerang</p>
            </div>
            
            <div class="margin-bottom-20">
                <h5 class="f-italic">Misi Anugerah Satwa</h5>
                <p><i class="fa fa-caret-right text-azure"></i> Memberikan pelayanan kesehatan dan perawatan hewan yang cepat ,tepat, ramah dan terpercaya</p>
                <p><i class="fa fa-caret-right text-azure"></i> Menjadi klinik hewan yang memiliki keunggulan di bidang layanan dan dukungan tenaga professional</p>
                <p><i class="fa fa-caret-right text-azure"></i> Menjadi klinik hewan yang menjungjung tinggi animal welfare</p>
            </div>

            <div class="margin-bottom-30">
                <h5 class="f-italic">Contact Anugerah Satwa</h5>
                <p><i class="fa fa-caret-right text-azure"></i> (021) 75884407</p>
                <p><i class="fa fa-caret-right text-azure"></i> Address: Jl. Raya Ciater Barat No.64, Rw. Buntu, Serpong, Kota Tangerang Selatan, Banten 15310</p>
            </div>
        </div>
    </div>
</div>