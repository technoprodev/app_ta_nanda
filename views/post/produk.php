<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = 'Produk';
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="padding-y-30">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">Produk</span>
            </div>

            <div class="margin-bottom-30">
                <h4 class="f-italic">Makanan Kucing</h4>

                <h5 class="text-azure">Me-O</h5>
                <p>Makanan kucing jenis ini di khususkan untuk kucing berbulu panjang seperti halnya kucing persia, anggora dan lainnya. Me-o umumnya memproduksi jenis makanan basah dan kering. Kelebihan makanan ini adalah nutrisi dan mineralnya yang sangat baik untuk jenis kucing berbulu panjang. Untuk kandungan mineral serta vitaminnya juga dapat berfungsi mencegah kerontokan pada bulu kucing dan menyelamatkan kucing anda dari bahaya menelan bulu yang rontok pada masa kerontokan. Makanan ini juga mengandung asam amino omega 3 dan 6 yang berfungsi merangsang kehalusan pada bulu dan kulit yang berkilau pada kucing. Untuk soal harga Me-O relatif sedang tidak terlalu mahal.</p>
                <p>Harga:
                    <ul>
                        <li>Me-O Persian Berat 1.5 Kilogram = 70.000</li>
                        <li>Me-O Adult Tuna/Chiken/Beef/ Berat 1.5 Kilogram = 65.000</li>
                        <li>Me-O Kitten Berat 1,5 Kilogram = 63.000</li>
                        <li>Me-O Tuna Berat 7 Kilogram = 280.000</li>
                    </ul>
                </p>

                <h5 class="text-azure">Royal Canin</h5>
                <p>Makanan jenis ini cukup mempunyai raputasi yang bagus, karena kelebihan makanan ini di percayai menangkal kerontokan pada bulu kucing yang di sebabkan faktor cuaca serta faktor iklim tertentu bahkan banyak orang mengatakan bahwa royal canin dapat melebatkan bulu kucing domestik atau kucing kampung.</p>
                <p>Banyak dokter hewan yang menyarankan cat lovers untuk menggunakan makanan jenis tersebut, karena kualitas nutrisi yang terkandung tidak di ragukan lagi, namun memang harga yang di tawarkan pun sebanding dengan kualitas yang diberikan. Royal canin umumnya terbagi menjadi beberapa bagian, secara khusus untuk kucing persia, anggora, dan serta kucing domestik, produk yang paling populer adalah royal canin kitten persian 32 dan persian adult.</p>
                <p>Harga:
                    <ul>
                        <li>Royal canin 30 – 900gr = 95.000 ( repack )</li>
                        <li>Royal canin 32 – 900gr = 95.000 ( repack )</li>
                        <li>Royal canin 30, 33 dan 36 dengan berat 2 kg = 200.000 ( fresh pack )</li>
                        <li>Royal canin 30, 33 dan 36 dengan berat 400 gram = 50.000 ( fresh pack )</li>
                    </ul>
                </p>

                <h5 class="text-azure">Pro Plan</h5>
                <p>Makanan ini sangat populer di kalangan pecinta kucing, umumnya banyak pecinta kucing khusus eropa yang menggunakan makanan jenis ini, proplan tergolong cat food super premium karena kualitas yang baik. Kelebihan pada makanan ini adalah selain mempunyai dua variasi di antara makanan basah dan kering. Makanan ini dapat memaksimalkan berat badan kucing untuk anda yang yang menyukai bentuk badan kucing yang gemuk, tetapi untuk anda yang menginginkan kucing dengan bulu yang lebat saya lebih merekomendasikan royal canin. Kandungan protein yang tinggi sekitar 41% membuat kucing anda cukup aktif, proplan juga akan berfungsi membuat poop kucing lebih padat dan tidak bau. Harganya cukup mahal karena memang kualitasnya yang tidak di ragukan lagi.</p>
                <p>Harga:
                    <ul>
                        <li>Proplan kitten berat 3 kilogram = 270.000</li>
                        <li>Proplan adult berat 7 kilogram = 530.000</li>
                        <li>Proplan adult berat 3 kilogram = 280.000</li>
                    </ul>
                </p>

                <h5 class="text-azure">Whiskas</h5>
                <p>Makanan ini ada dua tipe yakni makanan basah dan kering, kelebihan makanan ini mempunyai nutrisi dan protein yang relatif bagus, tapi banyak yang mengatakan jenis makanan ini juga tipe makanan cocok seperti Frieskie, yang dapat diartikan bisa membuat kucing menci-menci dan alergi jika dia merasa tidak cocok dengan makanan ini. Kelebihan lain makanan ini mempunyai bentuk yang lebih kecil dan lebih lembab dari jenis makanan lain, sehingga cukup mudah untuk di konsumsi oleh kucing manapun.</p>
                <p>Harga:
                    <ul>
                        <li>Junior Berat 1,5 Kilogram = Rp. 70.000</li>
                        <li>Adult Berat 1,5 Kilogram = Rp. 70.000</li>
                    </ul>
                </p>

                <h5 class="text-azure">Frieskies</h5>
                <p>Merk ini cukup populer di Indonesia karena friskies sering menjadi sponsor beberapa acara atau lomba binatang peliharaan. Sama seperti whiskas, friskies juga mudah ditemui di beberapa supermarket terdekat. Friskies sendiri tergolong cat food premium yang jika dibandingkan dengan makanan kucing super premium maka harga friskies terbilang cukup murah. Makanan ini mempunyai gizi yang cukup seimbang, tapi menurut kalangan pecinta kucing, jenis makanan ini cukup kontroversi karena banyak yang mengatakan jenis makanan tersebut mempunyai kandungan bahan yang tidak cocok untuk kucing, sehingga dapat mengakibatkan kucing menci-menci. Tapi sebenernya semua makanan kucing itu cocok-cocokan, nah mungkin saja jenis makanan ini dapat cocok dengan kucing kalian. Untuk jenis makanan ini terdapat dua variasi dengan adanya makanan basah dan kering.</p>
                <p>Harga:
                    <ul>
                        <li>FRISKIES Kitten Berat 1,5 Kilogram = 70.000</li>
                        <li>FRISKIES Adult Tuna/Ocean Fish/Gourmet Berat 1,5 Kilogram = 70.000</li>
                        <li>FRISKIES Wet Food Berat 400 Gram = 20.000</li>
                    </ul>
                </p>

                <h5 class="text-azure">Maxi</h5>
                <p>Maxi termasuk makanan kucing premium dengan harga yang relatif terjangkau, komposisi pada maxi diklaim memiliki protein 32% serta memiliki omega 3 dan 6, maxi premium cat food berbentuk biskuit atau makanan kering dengan diameter yang cukup kecil dengan komposisi ikan tuna. Selain kemasan pasaran maxi juga dapat dibeli dengan kemasan repack. Harga untuk maxi ukuran 500 gram adalah Rp.15.000 cukup murah untuk kalian yang ingin membeli ketengan.</p>


                <h4 class="margin-top-20 f-italic">Makanan Anjing</h4>

                <h5 class="text-azure">Royalcanin</h5>
                <p>Harga:
                    <ul>
                        <li>1,5 kilogram dog chihuahua 28 Rp. 200,000</li>
                        <li>1,5 kilogram dog shih tzu 24 Rp. 170,000</li>
                        <li>1,5 kilogram dog Yorkshire Terrier 28 Rp. 170,000</li>
                        <li>3 kilogram puppy babydog 28 Rp. 300,000</li>
                        <li>3 kilogram puppy mini junior Rp. 270,000</li>
                    </ul>
                </p>

                <h5 class="text-azure">Pro plan</h5>
                <p>Harga:
                    <ul>
                        <li>1 kilogram dog performance formula Rp. 50,000</li>
                        <li>1 kilogram dog shredded blend beef and rice formula Rp. 55,000</li>
                        <li>1 kilogram puppy chicken and rice formula Rp. 65,000</li>
                        <li>15,9 kilogram (35 lb) dog shredded blend beef and rice formula Rp. 520,000</li>
                        <li>17 kilogram (37,5 lb) dog performance formula Rp. 650,000</li>
                        <li>8,16 kilogram (18 lb) dog shredded blend beef and rice formula Rp. 310,000</li>
                        <li>8,16 kilogram (18 lb) puppy chicken and rice formula Rp. 350,000</li>
                        <li>8,16 kilogram (18 lb) puppy natural lamb and rice formula Rp. 350,000</li>
                        <li>8,16 kilogram (18 lb) puppy small breed formula Rp. 350,000</li>
                    </ul>
                </p>

                <h5 class="text-azure">Alpo</h5>
                <p>Harga:
                    <ul>
                        <li>1,5 kilogram adult 2 in 1 Rp. 60,000</li>
                        <li>1,5 kilogram adult beef, liver and vegetable Rp. 70,000</li>
                        <li>1,5 kilogram adult chicken, liver and vegetable Rp. 80,000</li>
                        <li>1,5 kilogram puppy beef and vegetable Rp. 72,000</li>
                        <li>10 kilogram adult beef, liver and vegetable Rp. 270,000</li>
                        <li>10 kilogram adult chicken, liver and vegetable Rp. 270,000</li>
                        <li>3 kilogram adult 2 in 1 Rp. 85,000</li>
                        <li>3 kilogram puppy beef and vegetable Rp. 85,000</li>
                        <li>500 gram puppy beef and vegetable Rp. 30,000</li>
                        <li>8 kilogram puppy beef and vegetable Rp. 250,000</li>
                        <li></li>
                    </ul>
                </p>

                <h5 class="text-azure">Pedigree</h5>
                <p>Harga:
                    <ul>
                        <li>1 kilogram adult beef Rp. 40,000</li>
                        <li>1 kilogram adult chicken with tasty liver Rp. 40,000</li>
                        <li>1,15 kilogram adult beef Rp. 60,000</li>
                        <li>1,15 kilogram adult chicken Rp. 60,000</li>
                        <li>1,5 kilogram puppy chicken and egg Rp. 72,000</li>
                        <li>1,8 kilogram adult beef Rp. 80,000</li>
                        <li>1,8 kilogram adult chicken with tasty liver Rp. 80,000</li>
                        <li>10 kilogram adult beef Rp. 270,000</li>
                        <li>10 kilogram adult chicken with tasty liver Rp.270,000</li>
                        <li>20 kilogram adult beef Rp. 480,000</li>
                        <li>20 kilogram adult chicken with tasty liver Rp. 480,000</li>
                        <li>3,2 kilogram adult beef Rp. 110,000</li>
                        <li>3,2 kilogram adult chicken with tasty liver Rp. 120,000</li>
                        <li>400 gram adult 5 kinds of meat Rp. 30,000</li>
                        <li>400 gram adult beef Rp. 35,000</li>
                        <li>400 gram adult chicken Rp. 35,000</li>
                        <li>400 gram puppy Rp. 35,000</li>
                        <li>480 gram adult beef Rp. 38,000</li>
                        <li>480 gram adult chicken with tasty liver Rp. 38,000</li>
                        <li>480 gram puppy chicken and egg Rp. 39,000</li>
                        <li>75 gram denta stix Rp. 24,000</li>
                        <li>86 gram denta stix Rp. 24,000</li>
                    </ul>
                </p>

                <h5 class="text-azure">Science diet</h5>
                <p>Harga:
                    <ul>
                        <li>2,04 kg (4,5 lb) puppy small bites Rp. 140,000</li>
                        <li>2,26 kg (5 lb) dog small bites Rp. 130,000</li>
                        <li>7,94 kg (17,5 lb) puppy lamb meal and rice recipe original Rp. 410,000</li>
                        <li>7,94 kg (17,5 lb) puppy original Rp. 380,000</li>
                        <li>7,94 kg (17,5 lb) puppy small bites Rp. 380,000</li>
                        <li>9,07 kg (20 lb) dog lamb meal and rice recipe original Rp. 430,000</li>
                        <li>9,07 kg (20 lb) dog lamb meal and rice recipe small bites Rp. 430,000</li>
                        <li>9,07 kg (20 lb) dog original Rp. 380,000</li>
                        <li>9,07 kg (20 lb) dog small bites Rp. 380,000</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
</div>