<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30">
                <div class="fs-26 margin-bottom-10"><?= $model['post']->title ?></div>
                <div class="f-bold margin-bottom-2">by <?= $model['post']->createdBy->name ?></div>
                <div class="">at <?= $model['post']->created_at ?></div>
            </div>

            <div class="margin-bottom-10">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/cover.jpg" class="rounded-xs" style="width: 100%; height: auto;">
            </div>

            <div class="margin-bottom-30 padding-x-20">
                <?= $model['post']->excerpt ?>
            </div>

            <div class="margin-bottom-50 fs-14">
                <?= $model['post']->content ?>
            </div>

            <div class="fs-14 f-bold margin-bottom-10">
                Lihat Lainnya
            </div>

            <div class="box margin-bottom-30 equal">
            <?php foreach ($model['posts'] as $key => $post) : ?>
                <div class="box-4 padding-left-0 padding-right-20">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/cover.jpg" class="rounded-xs" style="width: 100%; height: auto;">
                    <div class="fs-15 margin-y-10">
                        <?= Html::a($post->title, ['view', 'id' => $post->id], ['class' => 'text-grayest ']) ?>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>