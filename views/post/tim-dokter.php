<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="padding-y-30">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">Tim Dokter</span>
            </div>

            <div class="box box-break-sm box-space-lg margin-bottom-60 equal">
                <div class="box-4">
                    <div class="margin-bottom-15"><img src="http://www.klinikcantikhartini.com/wp-content/uploads/2015/07/11084700_1455931398031251_1852250384_n-300x300.jpg" class="rounded-xs text-center shadow" width="240px" height="auto"></img></div>
                    <div class="text-center f-bold f-italic">Drh. Afif Yuda Kusumah </div>
                </div>
                <div class="box-4">
                    <div class="margin-bottom-15"><img src="http://www.ahlikanker.com/wp-content/uploads/2016/11/dokter-cantik.jpg" class="rounded-xs text-center shadow" width="240px" height="auto"></img></div>
                    <div class="text-center f-bold f-italic">Drh. Febiola Rama Sari, M.si </div>
                </div>
                <div class="box-4">
                    <div class="margin-bottom-15"><img src="https://2.bp.blogspot.com/-4_lnWF3r-88/VvHlKcqgHXI/AAAAAAAAR8c/PeANDpC3XDE18Kt27nyeayV9C3_cp5AJQ/s400/Indah%2BKusuma.jpg" class="rounded-xs text-center shadow" width="240px" height="auto"></img></div>
                    <div class="text-center f-bold f-italic">Drh.Desi Akhira </div>
                </div>
                <div class="box-4">
                    <div class="margin-bottom-15"><img src="http://liputanutama.cvmanglayang.com/wp-content/uploads/2016/11/krestle-lailene-deomampo.jpg" class="rounded-xs text-center shadow" width="240px" height="auto"></img></div>
                    <div class="text-center f-bold f-italic">Drh.Dwi Wahyu Rochmawati </div>
                </div>
                <div class="box-4">
                    <div class="margin-bottom-15"><img src="http://abulyatama.ac.id/wp-content/uploads/2015/10/1431315882593.jpg" class="rounded-xs text-center shadow" width="240px" height="auto"></img></div>
                    <div class="text-center f-bold f-italic">Drh. Ana Neli Rahma</div>
                </div>
            </div>

        </div>
    </div>
</div>