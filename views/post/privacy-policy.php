<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = 'Privacy Policy';
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">Privacy Policy</span>
            </div>
            <div class="margin-bottom-50">
                <p>Adanya Kebijakan Privasi ini adalah komitmen nyata dari Anugerah Satwa, Pet Shop,Clinic and Grooming untuk menghargai dan melindungi setiap informasi pribadi Pengguna situs www.AnugerahSatwaGrooming.com.</p>

                <p>Kebijakan ini (beserta syarat-syarat penggunaan dari situs Anugerah Satwa, Pet Shop,Clinic and Grooming sebagaimana tercantum dalam "Syarat & Ketentuan" dan dokumen lain yang tercantum di situs Anugerah Satwa, Pet Shop,Clinic and Grooming ) menetapkan dasar atas segala data pribadi yang Pengguna berikan kepada Anugerah Satwa, Pet Shop,Clinic and Grooming atau yang Anugerah Satwa, Pet Shop,Clinic and Grooming kumpulkan dari Pengguna, kemudian akan diproses oleh Anugerah Satwa, Pet Shop,Clinic and Grooming mulai pada saat melakukan pendaftaran, mengakses dan menggunakan layanan Anugerah Satwa, Pet Shop,Clinic and Grooming. Harap memperhatikan ketentuan di bawah ini secara seksama untuk memahami pandangan dan kebiasaan Anugerah Satwa, Pet Shop,Clinic and Grooming berkenaan dengan data Anugerah Satwa, Pet Shop,Clinic and Grooming memperlakukan informasi tersebut.</p>

                <p>Dengan mengakses dan menggunakan layanan situs Anugerah Satwa, Pet Shop,Clinic and Grooming, Pengguna dianggap telah membaca, memahami dan memberikan persetujuannya terhadap pengumpulan dan penggunaan data pribadi Pengguna sebagaimana diuraikan di bawah ini.</p>

                <p>Kebijakan Privasi ini mungkin di ubah dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. Anugerah Satwa, Pet Shop,Clinic and Grooming menyarankan agar anda membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Anugerah Satwa, Pet Shop,Clinic and Grooming, maka pengguna dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi ini.
                Informasi Pengguna</p>

                <p>Anugerah Satwa, Pet Shop,Clinic and Grooming mengumpulkan informasi Pengguna dengan tujuan untuk memperoses dan memperlancar proses penggunaan situs Anugerah Satwa, Pet Shop,Clinic and Grooming. Tindakan tersebut telah memperoleh persetujuan Pengguna pada saat pengumpulan informasi.</p>

                <p>Anugerah Satwa, Pet Shop,Clinic and Grooming mengumpulkan informasi pribadi sewaktu Pengguna mendaftar ke Anugerah Satwa, Pet Shop,Clinic and Grooming , sewaktu Pengguna menggunakan layanan Anugerah Satwa, Pet Shop,Clinic and Grooming, sewaktu Pengguna mengunjungi halaman Anugerah Satwa, Pet Shop,Clinic and Grooming  atau halaman-halaman para mitra tertentu dari Anugerah Satwa, Pet Shop,Clinic and Grooming , dan sewaktu Anda menghubungi Anugerah Satwa, Pet Shop,Clinic and Grooming . Anugerah Satwa, Pet Shop,Clinic and Grooming  dapat menggabung informasi mengenai Pengguna yang kami miliki dengan informasi yang kami dapatkan dari para mitra usaha atau perusahaan-perusahaan lain.</p>

                <p>Sewaktu Anda mendaftar untuk menjadi Pengguna Anugerah Satwa, Pet Shop,Clinic and Grooming , maka Anugerah Satwa, Pet Shop,Clinic and Grooming  akan mengumpulkan data pribadi Pengguna, yakni nama lengkap, alamat e-mail, tempat dan tanggal lahir, nomor telepon yang dapat dihubungi, password dan informasi lainnya yang diperlukan. Dengan memberikan informasi / data tersebut, Anda memahami, bahwa Anugerah Satwa, Pet Shop,Clinic and Grooming  dapat meminta dan mendapatkan setiap informasi / data pribadi Pengguna untuk kesinambungan dan keamanan situs ini.</p>

                <p>Anugerah Satwa, Pet Shop,Clinic and Grooming  akan mengumpulkan dan mengolah keterangan lengkap mengenai transaksi atau penawaran atau hubungan financial lainnya yang Pengguna laksanakan melalui situs Anugerah Satwa, Pet Shop,Clinic and Grooming  dan mengenai pemenuhan pesanan Pengguna.</p>

                <p>Anugerah Satwa, Pet Shop,Clinic and Grooming  akan mengumpulkan dan mengolah data mengenai kunjungan Pengguna ke situs Anugerah Satwa, Pet Shop,Clinic and Grooming , termasuk namun tidak terbatas pada data lalu-lintas, data lokasi, weblog, tautan ataupun data komunikasi lainnya, apakah hal tersebut disyaratkan untuk tujuan penagihan atau lainnya, serta sumberdaya yang Pengguna akses.</p>

                <p>Pada saat Pengguna menghubungi Anugerah Satwa, Pet Shop,Clinic and Grooming , maka Anugerah Satwa, Pet Shop,Clinic and Grooming  menyimpan catatan mengenai korespondensi tersebut dan isi dari komunikasi antara Pengguna dan Anugerah Satwa, Pet Shop,Clinic and Grooming .</p>

                <p>Pengguna memahami dan menyetujui bahwa nama Pengguna dan daerah Kota (bukan alamat lengkap) Pengguna merupakan informasi umum yang tertera di halaman profile Anugerah Satwa, Pet Shop,Clinic and Grooming  Pengguna.</p>

                <p>Anugerah Satwa, Pet Shop,Clinic and Grooming  dengan sendirinya menerima dan mencatat informasi dari komputer dan browser Pengguna, termasuk alamat IP, informasi cookie Anugerah Satwa, Pet Shop,Clinic and Grooming , atribut piranti lunak dan piranti keras, serta halaman yang Pengguna minta.</p>

                <p>Setiap informasi / data Pengguna yang disampaikan kepada Anugerah Satwa, Pet Shop,Clinic and Grooming  dan/atau yang dikumpulkan oleh Anugerah Satwa, Pet Shop,Clinic and Grooming  dilindungi dengan upaya sebaik mungkin oleh perangkat keamanan teruji, yang digunakan oleh Anugerah Satwa, Pet Shop,Clinic and Grooming  secara elektronik.</p>

                <p>Kerahasiaan kata sandi atau password merupakan tanggung jawab masing-masing Pengguna. Anugerah Satwa, Pet Shop,Clinic and Grooming  tidak bertanggung jawab atas kerugian yang dapat ditimbulkan akibat kelalaian pengguna dalam menjaga kerahasiaan passwordnya.</p>
            </div>
        </div>
    </div>
</div>