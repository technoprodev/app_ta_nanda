<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom"><?= $model['postCategory']->name ?></span>
            </div>

            <?php if ($model['post']) : ?>
                <?php foreach ($model['post'] as $key => $post) : ?>
                <div class="box margin-bottom-30">
                    <div class="box-3 padding-0">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/cover.jpg" style="width: 100%; height: auto;">
                    </div>
                    <div class="box-9">
                        <div class="fs-20 margin-bottom-5">
                            <?= Html::a($post->title, ['index', 'id' => $post->id], ['class' => 'text-grayest ']) ?>
                        </div>
                        <div class="text-gray">
                            by <?= $post->createdBy->name ?> on <?= $post->created_at ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div>
                    We are sorry, no post found
                </div>
            <?php endif; ?>
            
        </div>
    </div>
</div>