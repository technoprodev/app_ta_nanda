<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="padding-y-30">
                <span class="fs-26 padding-y-10 padding-right-20 border-bottom">Term of Conditions</span>
            </div>
            <div class="margin-bottom-50">
                <h5 class="margin-top-20">Layanan darurat</h5>
                <p>Semua keadaan darurat pada jam kerja ditangani oleh Layanan Anugerah Satwa Pet Shop, Clinic and Grooming</p>
                <p>Ini adalah layanan mandiri yang dipekerjakan oleh diri kita sendiri dan beberapa praktik lokal lainnya untuk memberikan perawatan darurat terus menerus untuk hewan peliharaan pada akhir pekan dan hari libur. Ini dikelola oleh dokter hewan dan perawat berpengalaman dalam pengobatan darurat dan operasi yang hanya bekerja untuk layanan darurat. Layanan ini berbasis di Jl. Raya Ciater Barat No.64, Rw. Buntu, Serpong, Kota Tangerang Selatan, Banten 15310. Untuk semua keadaan darurat pada jam kerja, hubungi petugas darurat darurat langsung di (021) 75884407.</p>

                <h5 class="margin-top-20">Biaya</h5>
                <p>Semua biaya, makanan dan biaya obat dikenakan PPN pada tarif saat ini. Tingkat biaya ditentukan oleh waktu yang dihabiskan untuk sebuah kasus dan sesuai dengan obat-obatan, bahan, bahan habis pakai dan makanan yang digunakan. Biaya obat resep yang diisi dan / atau dijual oleh Klinik akan diberikan berdasarkan permintaan.</p>

                <h5 class="margin-top-20">Metode Pembayaran</h5>
                <p>Semua tagihan harus dilunasi pada akhir konsultasi, pelepasan hewan peliharaan Anda atau setelah pengumpulan obat / diet. Anda dapat menyelesaikan akun dengan menggunakan:</p>
                <p>-KAS</p>
                <p>-KARTU KREDIT / DEBIT - Switch, Solo, Mastercard, Visa, Delta</p>
                <p>Kami tidak menyediakan rekening kredit dan kami tidak dapat menerima pembayaran dalam bentuk cek.</p>

                <h5 class="margin-top-20">Perkiraan Biaya Pengobatan</h5>
                <p>Kami dengan senang hati akan memberikan perkiraan tertulis mengenai kemungkinan biaya pengobatan. Ingatlah bahwa perkiraan yang diberikan hanya dapat perkiraan - seringkali penyakit hewan peliharaan tidak akan mengikuti jalur konvensiona</p>

                <h5 class="margin-top-20">Keluhan dan Saran</h5>
                <p>Kami berharap Anda tidak perlu lagi mengeluh tentang standar pelayanan yang diterima dari Anugerah Satwa, Pet Shop,Clinic and Grooming, yang selalu kami pertahankan untuk mempertahankan setingkat tertinggi. Namun, jika Anda merasa ada sesuatu yang tidak Anda sukai, tolong sampaikan komentar Anda pada contoh pertama kepada manajer praktik. Setiap komentar yang membantu kami memperbaiki layanan kami selalu diterima, dan kami hanya dapat terus menawarkan layanan yang Anda inginkan jika kami menerima umpan balik, baik atau buruk, dari klien kami.</p>
                <p>Segala jenis kritik, saran, maupun keperluan lain dapat disampaikan ke Contact Us</p>

                <h5 class="margin-top-20">Pembaharuan</h5>
                <p>Syarat & ketentuan mungkin di ubah dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. Tokopedia menyarankan agar anda membaca secara seksama dan memeriksa halaman Syarat & ketentuan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Tokopedia, maka pengguna dianggap menyetujui perubahan-perubahan dalam Syarat & ketentuan</p>
            </div>
        </div>
    </div>
</div>